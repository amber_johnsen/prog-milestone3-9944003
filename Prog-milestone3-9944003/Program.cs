﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9944003
{
        class Program
        {
            static void Main(string[] args)
            {
                try
                {
                    Welcome();
                    userdeets();
                    Pizzalist();
                    Pizzaselect();
                    size();
                    sizeselect();
                    Another();
                }
                catch
                {

                }
            }
            static void Welcome()
            {
                Console.WriteLine("****************************");  //ordering system is used by employees POS
                Console.WriteLine("**                        **");
                Console.WriteLine("**  Welcome to Ambers     **");
                Console.WriteLine("**      Pizza shop        **");
                Console.WriteLine("**                        **");
                Console.WriteLine("****************************\n");
            }
            static void userdeets()
            {
                Client user1 = new Client();  //having option for these details to be entered heere or before money is taken

                Console.WriteLine("Please enter your name");
                user1.Name = Console.ReadLine();

                Console.WriteLine("Please enter your Number (e.g. 075780000)");
                user1.Number = int.Parse(Console.ReadLine());
                Console.WriteLine("");


            }
            static void Pizzalist() //ordering system ideally brings up option to order food and drink each time
            {
                Console.Clear();
                Console.WriteLine("What Pizza would you like?\n");

                var Pizzas = new List<string> { "1.Supreme", "2.Hawiian", "3.Meat Lovers", "4.Spicy", "5.Veg\n" };

                foreach (var p in Pizzas)
                {
                    Console.WriteLine(p);

                }

            }
            static List<string> pizzalist = new List<string> { "" };
            static void Pizzaselect()
            {



                Pizza p1 = new Pizza();
                Console.WriteLine("Please enter the pizza you would like\n");
                int answer = int.Parse(Console.ReadLine());
                do
                {
                    if (answer == 0)
                    {
                        Console.Clear();
                        Console.WriteLine("Please enter a valid selection");
                        Console.ReadLine();
                        Console.Clear();
                        Pizzalist();



                        Console.ReadLine();
                        Pizzaselect();

                    }
                    if (answer == 1)
                    {

                        pizzalist.Add("Supreme");

                    }
                    else if (answer == 2)
                    {

                        pizzalist.Add("Hawiian");


                    }
                    else if (answer == 3)
                    {

                        pizzalist.Add("Meat Lovers");

                    }
                    else if (answer == 4)
                    {

                        pizzalist.Add("Spicy");

                    }
                    else if (answer == 5)
                    {

                        pizzalist.Add("Veg");

                    }

                } while (answer < 0);
            }
            static void size() //pizza size and price needs to be in a dictionary
            {

                Console.Clear();
                Console.WriteLine("What size would you like?\n");

                var sizes = new List<string> { "1.Small $7", "2.Medium $9", "3.Large $11\n" };

                foreach (var s in sizes)
                {
                    Console.WriteLine(s);

                }

            }
            static List<string> pizzasizes = new List<string> { "" };
            static void sizeselect()
            {

                Pizza p2 = new Pizza();
                Console.WriteLine("Please enter the pizza size you would like\n");
                int answer = int.Parse(Console.ReadLine());

                do
                {
                    if (answer == 0)
                    {
                        Console.Clear();
                        Console.WriteLine("Please enter a valid selection");
                        Console.ReadLine();
                        Console.Clear();
                        size();

                    }
                    if (answer == 1)
                    {

                        pizzasizes.Add("small");
                        price.Add(7.00);
                    }
                    else if (answer == 2)
                    {

                        pizzasizes.Add("Medium");
                        price.Add(9.00);

                    }
                    else if (answer == 3)
                    {

                        pizzasizes.Add("Large");
                        price.Add(11.00);

                    }
                } while (answer < 0);

            }
            static void Another()
            {
                Console.WriteLine("Would you like to add another pizza? (Y/N)");
                var answer = Console.ReadLine();

                if (answer == "y" || answer == "Y")
                {
                    Pizzalist();
                    Pizzaselect();
                    size();
                    sizeselect();
                    Another();
                }
                else
                {
                    Console.Clear();
                    side();
                }

            }
            static void side() //a dictionary for drinks & sides be more effective
            {
                Console.WriteLine("All sides cost $3\n");
                Console.WriteLine("Would you like to add a side? (Y/N)");
                var answer = Console.ReadLine();

                if (answer == "y" || answer == "Y")
                {
                    sidesmenu();
                    Sidesselect();
                    anotherside();

                }
                else if (answer == "n" || answer == "N")
                {
                    payment();
                }

            }
            static void sidesmenu()
            {
                Console.Clear();

                Console.WriteLine("What side would you like?\n");

                var sides = new List<string> { "1.Pepsi", "2.Coke", "3.Fanta", "4.Chips", "5.Garlic Bread\n" };

                foreach (var x in sides)
                {
                    Console.WriteLine(x);
                }

            }
            static List<string> sideslist = new List<string> { "" };
            static void Sidesselect()
            {
                Sides s1 = new Sides();
                Console.WriteLine("choose the side you want?");
                int answer = int.Parse(Console.ReadLine());
                do
                {
                    if (answer == 0)
                    {
                        Console.Clear();
                        Console.WriteLine("Please enter a valid selection");
                        Console.ReadLine();
                        Console.Clear();
                        sidesmenu();


                    }
                    if (answer == 1)
                    {

                        sideslist.Add("Pepsi");
                        price.Add(3.00);

                    }
                    else if (answer == 2)
                    {

                        sideslist.Add("Coke");
                        price.Add(3.00);

                    }
                    else if (answer == 3)
                    {

                        sideslist.Add("Fanta");
                        price.Add(3.00);
                    }
                    else if (answer == 4)
                    {

                        sideslist.Add("Chips");
                        price.Add(3.00);
                    }
                    else if (answer == 5)
                    {

                        sideslist.Add("Garlic bread");
                        price.Add(3.00);
                    }

                } while (answer < 0); //program logic, calculations to be computed, What is displayed to user are seperate

            }
            static void anotherside()
            {
                Console.WriteLine("Would you like to add another side? (Y/N)");
                var answer = Console.ReadLine();

                if (answer == "y" || answer == "Y")
                {
                    sidesmenu();
                    Sidesselect();
                    anotherside();

                }
                else if (answer == "n" || answer == "N")
                {
                    payment();
                }
            }
            static List<double> price = new List<double> { };
            private static double total;

            static void payment()
            {
                try
                {

                    double total = price.Sum();
                }
                catch
                {
                }
                Console.Clear();
                Console.WriteLine("Your order today consists of:");
                foreach (var a in pizzalist.Zip(pizzasizes, (t, w) => new { t, w }))  //this could be changed to a tuple in future
                {
                    Console.WriteLine(a.t + " - " + a.w);
                }
                foreach (var x in sideslist)
                {
                    Console.WriteLine(x);
                }

                Console.WriteLine($"\nYour total order comes to: ${total}");
                Console.WriteLine("\n");

                Console.WriteLine("Enter the amount to cover the bill (e.g. 10)");
                double answer = int.Parse(Console.ReadLine());

                if (answer == total)
                {
                    Console.WriteLine("Thank you for coming to Amber's, please come again");
                }
                else if (answer < total)
                {
                    Console.WriteLine($"amount payed ${answer} but the bill came to ${total}\n");
                    Console.WriteLine($"you still owe\n");
                    Console.WriteLine($"${total - answer}");
                    Console.WriteLine("Please pay the rest of the bill");
                    double answer1 = int.Parse(Console.ReadLine());
                    if (answer1 == total)
                    {
                        Console.WriteLine("Thank you for coming to Amber's, please come again");
                    }
                    else if (answer1 > total)
                    {
                        Console.WriteLine($"amount payed ${answer1} but the bill came to ${total - answer}\n");
                        Console.WriteLine($"here is your change\n");
                        Console.WriteLine($"${answer + answer1 - total}");
                        Console.WriteLine("");
                        Console.WriteLine($"enjoy your food :)");
                    }
                    else if (answer1 < total)
                    {
                        Console.WriteLine("I am sorry you do not have enough money to continue with this order.  THank you, please come again");
                    }
                }
                else if (answer > total)
                {
                    Console.WriteLine($"amount payed ${answer} but the bill came to ${total}\n");
                    Console.WriteLine($"here is your change\n");
                    Console.WriteLine($"${answer - total}");
                    Console.WriteLine("");
                    Console.WriteLine($"enjoy your food :)");
                }


                Console.Read();

            }



        }

    }

    public class Client
    {
        public string Name;
        public double Number;
    }
    public class Pizza
    {
        public string Type;
        public string Size;
    }
    public class Sides
    {
        public string Name;
    }

